package com.qualitype.education.exercisedatabase;

import java.util.List;
import java.util.ArrayList;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.ManyToMany;

@Table
@Entity
public class AuthorEntity {

  private Long Id;
  private String firstName;
  private String lastName;

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "Id", updatable = false, nullable = false, unique = true)
  public Long getId() {
    return this.Id;
  }

  @ManyToMany(mappedBy = "authors")
  private List<BookEntity> books = new ArrayList<BookEntity>();

  public List<BookEntity> getBooks() {
    return this.books;
  }

  public void setBooks(List<BookEntity> books) {
    this.books = books;
  }

  @Column(name = "firstName", updatable = false, nullable = false)
  public String getFirstName() {
    return this.firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  @Column(name = "lastName", updatable = false, nullable = false)
  public String getLastName() {
    return this.lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }
}
