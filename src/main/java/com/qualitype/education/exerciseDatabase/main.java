package com.qualitype.education.exercisedatabase;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Main {



    public static void main(String[] args) {
       
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("DatabasePersistenceUnit");
        EntityManager em = factory.createEntityManager();


        
        AuthorEntity a = new AuthorEntity();

        a.setFirstName("Markus");
        a.setLastName("Miller");


        a.setFirstName("Martin");
        a.setLastName("Schmidt");

        em.getTransaction().begin();

        em.persist(a);

        em.getTransaction().commit();

        em.close();

        factory.close();


        //BookEntity b = new BookEntity();

    


    }
}