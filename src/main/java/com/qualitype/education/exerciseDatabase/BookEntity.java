package com.qualitype.education.exercisedatabase;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Table
@Entity
public class BookEntity {
  private Long Id;
  private String book;

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "Id", updatable = false, nullable = false)
  public Long getId() {
    return this.Id;
  }

  @Column(name = "book", updatable = false, nullable = false)
  public String getBook() {
    return this.book;
  }

  public void setBook(String book) {
    this.book = book;
  }

  @ManyToMany
  private List<AuthorEntity> authors = new ArrayList<AuthorEntity>();

  public List<AuthorEntity> getAuthors() {
    return this.authors;
  }

  public void setAuthors(List<AuthorEntity> authors) {
    this.authors = authors;
  }
}
